package sbu.cs.file;

import java.io.Serializable;

public class User implements Serializable {

    private String name;
    private String sid;
    private int age;

    public User(String name, String sid, int age) {
        this.name = name;
        this.sid = sid;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", sid='" + sid + '\'' +
                ", age=" + age +
                '}';
    }
}
