package sbu.cs.simplethread;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        threads.add(new Twitter());
        threads.add(new Twitter());
        threads.add(new Twitter());
        threads.add(new Twitter());
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }
}
