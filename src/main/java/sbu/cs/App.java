package sbu.cs;

import java.math.BigInteger;

public class App {

    public static void main(String[] args) {
//        System.out.println(pow(3, 100, 1000007));
//        System.out.println(pow(3, 100));
        System.out.println(factorial(10));
    }

    public static String pow(int base, int power) {
        BigInteger tmp = new BigInteger("1");
        for (int i = 0; i < power; i++) {
            tmp = tmp.multiply(new BigInteger(String.valueOf(base)));
        }
        return tmp.toString();
    }

    public static long pow(int base, int power, int mode) {
        long tmp = 1L;
        for (int i = 0; i < power; i++) {
            tmp *= base;
            tmp %= mode;
        }
        return tmp;
    }

    public static long factorial(int num) {
        if (num < 2) {
            return 1L;
        }
        return num * factorial(num - 1);
    }
}
