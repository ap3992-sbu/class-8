package sbu.cs.file;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        writeObject();
        readObject();
    }

    private static void readObject() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("users.txt")
        );
        User user = (User) objectInputStream.readObject();
        printUserDetail(user);
    }

    private static void writeObject() throws IOException {
        User user = new User("sahar", "97222001", 22);
        printUserDetail(user);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("users.txt")
        );
        objectOutputStream.writeObject(user);
    }

    private static void printUserDetail(User user) {
        System.out.println(user);
    }
}
